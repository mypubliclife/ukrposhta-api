<?php

namespace flinebux\Shipping;
use flinebux\Shipping\UkrposhtaApi;
/**
 * Created by PhpStorm.
 * User: WhiteCat636
 * Date: 29.10.2019
 * Time: 17:02
 */
class UkrPoshta_getData
{
    private $ukrposhtaObj;



    function __construct(){

        $bearer = '';
        $token  = '';

        $this->ukrposhtaObj = new UkrposhtaApi($bearer,$token);
    }

    /* @return Cache | CacheRedis  Return cache object */
    protected static function getCache()
    {
        if (defined('USE_REDIS_CACHE') && USE_REDIS_CACHE === true) {
            require_once __DIR__ . "/../ext-library/cache-redis.php";

            if (class_exists('CacheRedis')) {
                $cache = new CacheRedis();
            }
        }

        if (!isset($cache)) {
            $cache = new Cache();
        } // Cache - original file cache

        return $cache;
    }
    public function getCities(){
        $cache = self::getCache();

        $cacheKey = 'np.tree.cities';

        $cities = $cache->get($cacheKey);

        if (!$cities) {
            $cities = [];

            Config\Config_init::initConfig(); //init config NP

            $cityObj = new Address_getCities();

            $citiesObj = Address::getCities($cityObj); // get cities from NP

            if (!empty($citiesObj->data)) {
                foreach ($citiesObj->data as $city) {
                    $cities[] = [
                        'label' => $city->DescriptionRu,
                        'value' => $city->Ref
                    ];
                }

                $cities = self::sortCitiesByList($cities);

                $cache->set($cacheKey, $cities, EXPIRE_DAY);
            } elseif (!empty($citiesObj->errors)) {
                $cities['errors'] = $citiesObj->errors;
            }
        }

        return !empty($cities) ? $cities : [];
    }
    public function getWarehouses(){

    }
    public function getStreets(){

    }
    public function getWarehousesObj(){

    }
    public function getStreetsObj(){

    }



}